cmake_minimum_required(VERSION 3.27)

project(opengl VERSION 0.1)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(thirdparty_dir "${PROJECT_SOURCE_DIR}/thirdparty/")

add_subdirectory("${thirdparty_dir}glfw")
add_subdirectory("${thirdparty_dir}glad")
add_subdirectory("${thirdparty_dir}stb")

set(source_dir "${PROJECT_SOURCE_DIR}/src")
file(GLOB_RECURSE source_files "${source_dir}/*.cpp")
add_executable(opengl "${source_files}")

target_include_directories(opengl
    PRIVATE "${thirdparty_dir}glm")
target_link_libraries(opengl
    PRIVATE glfw glad stb_image)
