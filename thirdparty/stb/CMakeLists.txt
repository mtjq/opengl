add_library(stb_image "${CMAKE_CURRENT_SOURCE_DIR}/stb_image.cpp")

target_include_directories(glad
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
