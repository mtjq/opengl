#pragma once

#include <string>
#include <glm/glm.hpp>

class Shader {
 public:
  unsigned int id_;

  Shader(const char* vertex_path, const char* fragment_path);
  ~Shader();
  void Use();
  void SetBool(const std::string &name, bool value) const;
  void SetInt(const std::string &name, int value) const;
  void SetFloat(const std::string &name, float value) const;
  void Set4Float(
      const std::string &name, float v1, float v2, float v3, float v4) const;
  void SetMatrix4(const std::string &name, const glm::mat4 &mat) const;
};
