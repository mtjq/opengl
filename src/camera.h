#pragma once

#include <glm/glm.hpp>

enum Direction {
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT,
};

class FlyCamera {
 public:
  FlyCamera();
  void Move(Direction dir, float delta);
  void Rotate(float x_offset, float y_offset);
  glm::mat4 lookat() const;

 private:
  glm::vec3 position_;
  glm::vec3 forward_;
  glm::vec3 up_;
  glm::mat4 lookat_;
  float yaw_;
  float pitch_;
  float move_speed_;
  float rotate_speed_;

  void UpdateLookat();
};
