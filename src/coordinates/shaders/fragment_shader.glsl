#version 460 core
in vec2 tex_coord;

out vec4 FragColor;

uniform float texture_mix;
uniform sampler2D texture0;
uniform sampler2D texture1;

void main() {
  vec2 tex_coord1 = vec2(1.0 - tex_coord.x, tex_coord.y);
  FragColor = mix(
    texture(texture0, tex_coord), texture(texture1, tex_coord1), texture_mix);
}
