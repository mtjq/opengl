#version 460 core
out vec4 FragColor;

in vec3 color;
in vec2 tex_coord;

uniform float texture_mix;
uniform sampler2D texture0;
uniform sampler2D texture1;

void main() {
  vec2 tex_coord1 = vec2(1.0 - tex_coord.x, tex_coord.y);
  FragColor = mix(
    texture(texture0, tex_coord), texture(texture1, tex_coord1), texture_mix
    )
    * vec4(color, 1.0);
}
