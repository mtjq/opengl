#include "camera.h"

#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>

FlyCamera::FlyCamera()
    : position_{0.0, 0.0, 10.0},
      forward_{0.0, 0.0, -1.0},
      up_{0.0, 1.0, 0.0},
      yaw_{-90.0f},
      pitch_{0.0f},
      move_speed_{10.0f},
      rotate_speed_{0.1f} {
  UpdateLookat();
}

void FlyCamera::Move(Direction dir, float delta) {
  switch (dir) {
    case FORWARD:
      position_ += delta * move_speed_ * forward_;
      break;
    case BACKWARD:
      position_ -= delta * move_speed_ * forward_;
      break;
    case LEFT:
      position_ += delta * move_speed_
        * glm::normalize(glm::cross(up_, forward_));
      break;
    case RIGHT:
      position_ -= delta * move_speed_
        * glm::normalize(glm::cross(up_, forward_));
      break;
    default:
      return;
  }
  UpdateLookat();
}

void FlyCamera::Rotate(float x_offset, float y_offset) {
  yaw_ += rotate_speed_ * x_offset;
  pitch_ += rotate_speed_ * y_offset;
  pitch_ = std::clamp(pitch_, -89.0f, 89.0f);
  glm::vec3 direction;
  direction.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
  direction.y = sin(glm::radians(pitch_));
  direction.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
  forward_ = glm::normalize(direction);
  UpdateLookat();
}

glm::mat4 FlyCamera::lookat() const {
  return lookat_;
}

void FlyCamera::UpdateLookat() {
  lookat_ = glm::lookAt(position_, position_ + forward_, up_);
}
